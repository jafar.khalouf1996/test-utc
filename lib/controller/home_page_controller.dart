import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:utc_test/core/constant/color.dart';
import 'package:utc_test/data/model/models/alarm_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../core/class/notification/local_notification.dart';

class HomeScreenController extends GetxController {
  String? fctoken;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
//alarm page
  final TextEditingController titleController = TextEditingController();
  final TextEditingController noteController = TextEditingController();
  DateTime selectedDateforaddalarm = DateTime.now();
  String endTime = "9:30 pm";
  String startTime = DateFormat("hh:mm a").format(DateTime.now()).toString();

  DateTime selectedDate = DateTime.now();
  var alarmList = <AlarmModel>[];
  @override
  void onInit() {
    FirebaseMessaging.instance.getToken().then((value) {
      fctoken = value;
    });
    getalarms();

    super.onInit();
  }

  getalarms() async {
    firestore.collection("alarms").snapshots().listen((event) {
      alarmList = [];
      event.docs.forEach((element) {
        alarmList.add(AlarmModel(
          title: element['title'],
          date: element['date'],
          note: element['note'],
          startTime: element['startTime'],
          endTime: element['endTime'],
        ));
      });
      update();
    });

    // await alarmsref.get().then((value) {
    //   value.docs.forEach((element) {

    //     print("=====================alarmList============");
    //     print(alarmList);
    //     // alarmList.add(AlarmModel.fromJson(Map.fromIterable(iterable)));
    //     // print(element.data());
    //   });
    // });
  }

  getDateFromUser(BuildContext context) async {
    DateTime? pickerDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2121));

    if (pickerDate != null) {
      selectedDateforaddalarm = pickerDate;
      update();
    } else {}
  }

  getTimeFromUser(bool isStartTime, BuildContext context) async {
    TimeOfDay? pickedTime = await showTimePicker(
        initialEntryMode: TimePickerEntryMode.input,
        context: context,
        initialTime: TimeOfDay(
            hour: int.parse(startTime.split(":")[0]),
            minute: int.parse(startTime.split(":")[1].split(" ")[0])));
    // ignore: use_build_context_synchronously
    String? formatedTime = pickedTime?.format(context);
    if (pickedTime == null) {
    } else if (isStartTime == true) {
      startTime = formatedTime!;
      update();
    } else if (isStartTime == false) {
      endTime = formatedTime!;
      update();
    }
  }

  validateData() {
    if (noteController.text.isNotEmpty && titleController.text.isNotEmpty) {
      NotificationController.createNewNotification(
        AlarmModel(
            title: titleController.text,
            date: DateFormat.yMd().format(selectedDateforaddalarm),
            note: noteController.text,
            endTime: endTime,
            startTime: startTime),
      );
      NotificationController.scheduleNewNotification(
        selectedDateforaddalarm.day,
        int.parse(startTime.split(":")[0]),
        int.parse(startTime.split(":")[1].split(" ")[0]),
        AlarmModel(
            title: titleController.text,
            date: DateFormat.yMd().format(selectedDateforaddalarm),
            note: noteController.text,
            endTime: endTime,
            startTime: startTime),
      );
      addDataToFireStore();
      Get.back();
    } else if (titleController.text.isEmpty || noteController.text.isEmpty) {
      Get.snackbar("Required", "All fields are required !!",
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.white,
          colorText: AppColors.primary,
          icon: Icon(Icons.warning_amber_rounded));
    }
  }

  addDataToFireStore() async {
    FirebaseFirestore.instance.collection("alarms").add({
      'title': titleController.text,
      'note': noteController.text,
      'date': DateFormat.yMd().format(selectedDateforaddalarm),
      'startTime': startTime,
      'endTime': endTime
    });
  }
}
