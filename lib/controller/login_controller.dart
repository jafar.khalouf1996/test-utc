import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:utc_test/core/constant/routes.dart';
import 'package:utc_test/view/screen/home_page.dart';

class LoginController extends GetxController {
  final TextEditingController usernamecontroller = TextEditingController();

  final TextEditingController passwordcontroller = TextEditingController();
  login() async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(
            email: usernamecontroller.text,
            password: passwordcontroller.text,
          )
          .then((value) => {Get.toNamed(AppRoute.homepage)});
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        print("====================================");
        Get.snackbar("error", "The password provided is too weak.",
            colorText: const Color.fromARGB(255, 255, 17, 0));
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
        print("==============================");
        Get.snackbar("error", "The account already exists for that email.",
            colorText: const Color.fromARGB(255, 255, 17, 0));
      } else {
        Get.snackbar("error", "email or password not correct",
            colorText: const Color.fromARGB(255, 253, 17, 0));
      }
    } catch (e) {
      print(e);
    }
  }
}
