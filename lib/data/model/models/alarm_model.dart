class AlarmModel {
  int? id;
  String? title;
  String? note;
  String? date;
  String? startTime;
  String? endTime;

  AlarmModel({
    this.id,
    this.title,
    this.note,
    this.date,
    this.startTime,
    this.endTime,
  });

  AlarmModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    note = json['note'];
    date = json['date'];
    startTime = json['startTime'];
    endTime = json['endTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"] = id;
    data["title"] = title;
    data["note"] = note;
    data["date"] = date;
    data["startTime"] = startTime;
    data["endTime"] = endTime;

    return data;
  }
}
