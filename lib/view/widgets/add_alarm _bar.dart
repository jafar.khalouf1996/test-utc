import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:utc_test/view/screen/add_alarm.dart';
import 'package:utc_test/view/widgets/button.dart';

class AlarmBarWidget extends StatelessWidget {
  const AlarmBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  DateFormat.yMMMMd().format(DateTime.now()),
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[400]),
                ),
                const Text(
                  "Today",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                )
              ],
            ),
          ),
          MyButton(
              label: "+ Add alarm",
              ontap: () async {
                await Get.to(() => const AddAlarmScreen());
                // _taskController.getTasks();
              })
        ],
      ),
    );
  }
}
