import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:utc_test/controller/login_controller.dart';
import 'package:utc_test/view/widgets/button.dart';
import 'package:utc_test/view/widgets/input_faild.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var loginController = Get.put(LoginController());
    return SafeArea(
        child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            "Login",
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: MyInputFaild(
              title: "userName",
              hint: "enter user name",
              controller: loginController.usernamecontroller,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: MyInputFaild(
              title: "password",
              hint: "enter password",
              controller: loginController.passwordcontroller,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          MyButton(
            label: "Login",
            ontap: () => loginController.login(),
          )
        ],
      ),
    ));
  }
}
