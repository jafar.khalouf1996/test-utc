import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:utc_test/controller/home_page_controller.dart';
import 'package:utc_test/data/model/models/alarm_model.dart';
import 'package:utc_test/view/widgets/add_alarm%20_bar.dart';
import 'package:utc_test/view/widgets/alarm_tile.dart';
import 'package:utc_test/view/widgets/date_bar_widget.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../../core/class/notification/local_notification.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});
  final homecontroller = Get.put(HomeScreenController());
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          floatingActionButton: GestureDetector(
            onTap: () => NotificationController.cancelNotifications(),
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 236, 16, 0),
                    borderRadius: BorderRadius.circular(360)),
                width: 250,
                height: 50,
                child: const Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Cancel all notifications",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Icon(Icons.notifications_off_outlined)
                  ],
                ))),
          ),
          backgroundColor: Colors.white,
          body: Column(
            children: [
              const AlarmBarWidget(),
              GetBuilder<HomeScreenController>(
                  builder: (controller) => DateBarWidget(onDateChange: (date) {
                        controller.selectedDate = date;
                        controller.update();
                      })),
              const SizedBox(
                height: 10,
              ),
              _showTasks(),
            ],
          )),
    );
  }

  _showTasks() {
    return Expanded(
        child: GetBuilder<HomeScreenController>(
            builder: (controller) => ListView.builder(
                itemCount: homecontroller.alarmList.length,
                itemBuilder: (_, index) {
                  AlarmModel alarm = homecontroller.alarmList[index];

                  if (alarm.date ==
                      DateFormat.yMd().format(homecontroller.selectedDate)) {
                    return AnimationConfiguration.staggeredList(
                        position: index,
                        child: SlideAnimation(
                            child: FadeInAnimation(
                                child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                // _showButtomSheet(context, task);
                              },
                              child: AlarmTitle(alarm),
                            )
                          ],
                        ))));
                  } else {
                    return Container();
                  }
                })));
  }
}
