import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:utc_test/controller/home_page_controller.dart';
import 'package:utc_test/view/widgets/button.dart';
import 'package:utc_test/view/widgets/input_faild.dart';

class AddAlarmScreen extends StatelessWidget {
  const AddAlarmScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
          onTap: () {
            Get.back();
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: SingleChildScrollView(
            child: GetBuilder<HomeScreenController>(
          builder: (controller) => Column(
            children: [
              const Text(
                "Add Alarm",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              MyInputFaild(
                title: "Title",
                hint: "Enter your title",
                controller: controller.titleController,
              ),
              MyInputFaild(
                title: "Note",
                hint: "Enter your Note",
                controller: controller.noteController,
              ),
              MyInputFaild(
                title: "Date",
                hint:
                    DateFormat.yMd().format(controller.selectedDateforaddalarm),
                widget: IconButton(
                  onPressed: () {
                    controller.getDateFromUser(context);
                  },
                  icon: const Icon(Icons.calendar_today_outlined,
                      color: Colors.grey),
                ),
              ),
              Row(
                children: [
                  Expanded(
                      child: MyInputFaild(
                    title: "start Time",
                    hint: controller.startTime,
                    widget: IconButton(
                        onPressed: () {
                          controller.getTimeFromUser(true, context);
                        },
                        icon: const Icon(
                          Icons.access_time_rounded,
                          color: Colors.grey,
                        )),
                  )),
                  const SizedBox(
                    width: 12,
                  ),
                  Expanded(
                      child: MyInputFaild(
                    title: "end Time",
                    hint: controller.endTime,
                    widget: IconButton(
                        onPressed: () {
                          controller.getTimeFromUser(false, context);
                        },
                        icon: const Icon(
                          Icons.access_time_rounded,
                          color: Colors.grey,
                        )),
                  ))
                ],
              ),
              const SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  MyButton(
                      label: "Create Alarm",
                      ontap: () => controller.validateData())
                ],
              )
            ],
          ),
        )),
      ),
    );
  }
}
