import 'package:get/get.dart';
import 'package:utc_test/view/screen/add_alarm.dart';
import 'package:utc_test/view/screen/home_page.dart';
import 'package:utc_test/view/screen/login_screen.dart';

import 'core/constant/routes.dart';

List<GetPage<dynamic>>? routes = [
  GetPage(name: "/", page: () => const LoginScreen()),
  GetPage(name: AppRoute.homepage, page: () => HomePage()),
  GetPage(name: AppRoute.addalarm, page: () => const AddAlarmScreen()),
];
