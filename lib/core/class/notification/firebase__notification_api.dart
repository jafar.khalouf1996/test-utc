import 'package:firebase_messaging/firebase_messaging.dart';

Future<void> handleBackgroundMessage(RemoteMessage message) async {
  print('Title: ${message.notification?.title}');
  print('Body: ${message.notification?.body}');
  print('Payload: ${message.data}');
}

class FirebaseApi {
  final _firebaseMessaging = FirebaseMessaging.instance;

  void handleMessage(RemoteMessage? message) {
    print("==============one============");
    if (message == null) return;
    print("==============tow============");
  }

  Future initPushNotification() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
            alert: true, badge: false, sound: true);
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((value) => handleMessage(value));
    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      print("===================onMessageOpenedApp====================");
      handleMessage(event);
    });
    FirebaseMessaging.onBackgroundMessage(
        (message) => handleBackgroundMessage(message));
    FirebaseMessaging.onMessage.listen((message) {
      print("=================================");
      print(message.notification?.body);
      final notification = message.notification;
      if (notification == null) return;
      // _localNotifications.show(
      //     notification.hashCode,
      //     notification.title,
      //     notification.body,
      //     NotificationDetails(
      //         android: AndroidNotificationDetails(
      //             _androidChannel.id, _androidChannel.name,
      //             channelDescription: _androidChannel.description,
      //             icon: '@drawable/ic_launcher')),
      //     payload: jsonEncode(message.toMap()));
    });
  }

  Future<void> initNotification() async {
    await _firebaseMessaging.requestPermission();
    final fCMToken = await _firebaseMessaging.getToken();
    print(
        "===========================fctooken================================");
    print("Token: $fCMToken");
    initPushNotification();

    // initLocalNotification();
    inittialIphonenotification();
  }

  inittialIphonenotification() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }
}
